@api
Feature: Api Task
#  Background:
#    Given Http Get request is sent to "https://jsonplaceholder.typicode.com/posts"
#    When Status Code is 200
  Scenario Outline:
    Given Http Get request is sent to "https://jsonplaceholder.typicode.com/posts"
    When Status Code is 200
    Then According to response, user <userId> should have <numposts> posts
    Examples:
    |userId|numposts|
    |  5   |   10   |
    |  7   |   10   |
    |  9   |   10   |

    Scenario: Uniqueness of post Ids
      Given Http Get request is sent to "https://jsonplaceholder.typicode.com/posts"
      When Status Code is 200
      Then Each post id should be unique