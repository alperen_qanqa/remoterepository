@ui
Feature: Shop By Departments on Amazon.com
  Scenario: Check whether any department link is broken or not
    Given user goes to amazon
    When user clicks little hamburger menu on the left
    And clicks on department names one by one
