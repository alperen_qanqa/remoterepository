package test.amazon.pages;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import test.amazon.utilities.Driver;

import java.util.List;

public class ProductPage extends Homepage{

    @FindBy(xpath = "//span[normalize-space(text())=\"Your selected delivery location is beyond seller's shipping coverage for this item. Please choose a different delivery location or purchase from another seller.\"]")
    public List<WebElement> shippingError;

    @FindBy(css = "span#productTitle")
    public WebElement productTitle;

    @FindBy(xpath = "//td[text()='New Price:']")
    public List<WebElement> newPrice;
    @FindBy(xpath = "//td[text()='You Save:']")
    public List<WebElement> youSave;

    @FindBy(id="add-to-cart-button")
    public List<WebElement> addToCartButton;


    public boolean isDiscountedOrNotShippable(){
        waitForPageLoad();
        return addToCartButton.size()==0 || newPrice.size()>0 || youSave.size()>0 ;
    }


    @FindBy(xpath = "(//*[normalize-space(text())='Added to Cart'])[1]")
    public WebElement addedToCart;

    public boolean isAddedToCart(){
        try {
            WebDriverWait wait = new WebDriverWait(Driver.get(),5000);
            wait.until(ExpectedConditions.visibilityOf(addedToCart));
        }catch (ElementNotVisibleException exception){
            return false;
        }

        return true;
    }
}
