package test.amazon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CartPage extends Homepage{

    @FindBy(css = "span.a-truncate-full")
    public List<WebElement> productsInCart;
    public String cartUrl = "https://www.amazon.com/gp/cart/view.html?ref_=nav_cart";
    public Set<String> productTitlesInCart(){
        Set<String> titles = new HashSet<>();
        for (WebElement product: productsInCart) {
            titles.add(product.getAttribute("innerHTML").trim());
        }
        return titles;
    }
}
