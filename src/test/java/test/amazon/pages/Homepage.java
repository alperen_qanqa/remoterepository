package test.amazon.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import test.amazon.utilities.Driver;

import java.util.ArrayList;
import java.util.List;

public class Homepage {

    public Homepage(){
        PageFactory.initElements(Driver.get(),this);
    }

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchBox;

    @FindBy(id = "nav-search-submit-button")
    private WebElement searchButton;

    @FindBy(id = "nav-cart")
    public List<WebElement> cart;

    @FindBy(id = "nav-hamburger-menu")
    public WebElement AllMenuOntheLeft;

    @FindBy(xpath = "//div[text()='shop by department']/..")
    public WebElement shopByDepartment;

    @FindBy(xpath = "//ul[contains(@class,'hmenu-visible')]//a[contains(@class,'hmenu-back-button')]")
    public WebElement backToMainMenu;

    @FindBy(xpath = "//ul[contains(@class,'hmenu-visible')]//a[@class='hmenu-item']")
    public List<WebElement> subCategories;


    public void clickOnDepartment(String mainDepartmentName){
        WebElement mainDepartment = Driver.get().findElement(By.xpath("//a[@class='hmenu-item']/div[text()=\""+mainDepartmentName+"\"]/../.."));
        mainDepartment.click();
    }

    public List<String> departmentNames(){
        List<String> departments = new ArrayList<>();
        WebElement singleDepartmentElement = getNextSiblingElement(Driver.get(),shopByDepartment);
        while (true){
            String deptName = singleDepartmentElement.getText().trim();
            if(!deptName.trim().equals("")){
                departments.add(deptName);
            }
            singleDepartmentElement = getNextSiblingElement(Driver.get(),singleDepartmentElement);
            if (singleDepartmentElement.getTagName().equalsIgnoreCase("ul")){
                getNextSiblingElement(Driver.get(),singleDepartmentElement).click();
                List<WebElement> otherDepartments = getChildElements(Driver.get(),singleDepartmentElement);
                for (WebElement department: otherDepartments) {
                    if (!department.getText().trim().equals("")) {
                        departments.add(department.getText());
                    }
                }
                break;
            };

        }
        return departments;

    }
    public void clickWithJS(WebElement element) {
        ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView(true);", element);
        ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].click();", element);
    }

    public List<WebElement> getChildElements(WebDriver driver, WebElement element) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        List<WebElement> childElements = new ArrayList<>();
        if (hasChildElement(driver, element)) {
            childElements = (List<WebElement>) jsExecutor.executeScript("return arguments[0].children", element);
        }
        return childElements;
    }

    public static boolean hasChildElement(WebDriver driver, WebElement element) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        long childElementCount = (long) jsExecutor.executeScript("return arguments[0].childElementCount", element);
        return childElementCount > 0;
    }
    public static WebElement getNextSiblingElement(WebDriver driver, WebElement element) {
        return (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].nextElementSibling;", element);
    }
    public static WebElement getPreviousSiblingElement(WebDriver driver, WebElement element) {
        return (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].previousElementSibling;", element);
    }






    public void search(String product){
        searchBox.sendKeys(product);
        searchButton.click();

        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };

        try {
            WebDriverWait wait = new WebDriverWait(Driver.get(), 10);
            wait.until(expectation);
        } catch (Throwable error) {
            error.printStackTrace();
        }
    }

    public void waitForPageLoad(){
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };
        try {
            WebDriverWait wait = new WebDriverWait(Driver.get(), 10);
            wait.until(expectation);
        } catch (Throwable error) {
            error.printStackTrace();
        }
    }
    public static void scrollToElement(WebElement element) {
        ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView(true);", element);
    }


}
