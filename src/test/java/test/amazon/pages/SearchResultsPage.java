package test.amazon.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import test.amazon.utilities.ConfigurationReader;
import test.amazon.utilities.Driver;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class SearchResultsPage extends Homepage{


    //products with link, text will be taken in its own product page later
    @FindBy(css = "div.s-main-slot>div[data-component-type='s-search-result'] a.a-link-normal.s-no-outline")
    public List<WebElement> regularResults;

    //Contains product titles and parent element of each contains link for product
    @FindBy(xpath = "//div[contains(@id,'anonCarousel4')]//li[@class='a-carousel-card']//h2/a/span")
    public List<WebElement> someResultsStandNextToEachOther1;
    //Contains product titles and parent element of each contains link for product
    @FindBy(xpath = "//div[contains(@id,'anonCarousel5')]//li[@class='a-carousel-card']//h2/a/span")
    public List<WebElement> someResultsStandNextToEachOther2;

    public static WebElement getParentElement(WebDriver driver, WebElement element) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;

        return (WebElement) jsExecutor.executeScript("return arguments[0].parentElement;", element);
    }




}
