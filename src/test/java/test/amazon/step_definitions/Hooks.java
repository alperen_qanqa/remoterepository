package test.amazon.step_definitions;

import cucumber.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import test.amazon.utilities.Driver;

import java.util.concurrent.TimeUnit;

public class Hooks {
    @Before("@ui")
    public void setUp() {
        Driver.get().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Driver.get().manage().window().maximize();

    }

    @After("@ui")
    public void tearDown(Scenario scenario) {

        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) Driver.get()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }
        Driver.closeDriver();
    }
}
