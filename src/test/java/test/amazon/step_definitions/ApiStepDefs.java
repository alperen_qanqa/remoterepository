package test.amazon.step_definitions;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.*;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;

public class ApiStepDefs {

    Response response;

    @Given("Http Get request is sent to {string}")
    public void http_Get_request_is_sent_to(String api_url) {
        response = given().accept(ContentType.JSON).baseUri(api_url).when().get();
    }

    @When("Status Code is {int}")
    public void status_Code_is(Integer statusCode) {
        Assert.assertEquals(response.statusCode(),200);
    }

    @Then("According to response, user {int} should have {int} posts")
    public void according_to_response_user_should_have_posts(int userId, int numberOfPostsPerUser) {
        List<Map<String, Object>> actualResponse = response.jsonPath().getList("");
        int actualPostNum = 0;
        for (Map<String, Object> userData : actualResponse) {
            if (userData.get("userId").equals(userId)) {
                actualPostNum++;
            }
        }
        Assert.assertEquals(numberOfPostsPerUser,actualPostNum);
    }

    @Then("Each post id should be unique")
    public void eachPostIdShouldBeUnique() {
        List<Integer> postIds = response.jsonPath().getList("id");
        List<Map<String,Integer>> duplicateResults = new ArrayList<>();
        for (int i = 0; i < postIds.size(); i++) {
            if (postIds.indexOf(postIds.get(i)) != postIds.lastIndexOf(postIds.get(i))) {
                Map<String,Integer> duplicateResult = new HashMap<>();
                duplicateResult.put("id",postIds.get(i));
                duplicateResults.add(duplicateResult);
            }
        }
        Assert.assertEquals("There are duplicate results",0,duplicateResults.size());
    }
}
