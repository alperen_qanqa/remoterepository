package test.amazon.step_definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import test.amazon.pages.CartPage;
import test.amazon.pages.Homepage;
import test.amazon.pages.ProductPage;
import test.amazon.pages.SearchResultsPage;
import test.amazon.utilities.ConfigurationReader;
import test.amazon.utilities.Driver;

import java.util.*;

public class ShoppingCartSteps {
    Homepage homepage = new Homepage();
    SearchResultsPage searchResultsPage = new SearchResultsPage();
    CartPage cartPage = new CartPage();
    ProductPage productPage = new ProductPage();
    List<String> allProductLinks = new ArrayList<>();
    Set<String> expectedProductTitles = new HashSet<>();

    @Given("user searches laptop in amazon.com")
    public void user_searches_laptop_in_amazon_com() {
        Driver.get().get(ConfigurationReader.get("url"));
        homepage.search("laptop");
    }

    @When("user adds all non-discounted ones on the first page of results to the cart")
    public void user_adds_all_non_discounted_ones_on_the_first_page_of_results_to_the_cart() throws InterruptedException {
        for (WebElement product : searchResultsPage.regularResults) {
            allProductLinks.add(product.getAttribute("href"));
        }
        for (String productLink : allProductLinks) {
            Driver.get().get(productLink);
            if (!productPage.isDiscountedOrNotShippable()) {
                String productTitle = productPage.productTitle.getAttribute("innerHTML").trim();
                System.out.println(productTitle);
                ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].click();", productPage.addToCartButton.get(0));
                //productPage.addToCartButton.get(0).click();
                System.out.println("add button clicked ");
                expectedProductTitles.add(productTitle);
                Thread.sleep(3000);
            }
        }
    }

    @Then("user should see what is exactly selected on the cart")
    public void user_should_see_what_is_exactly_selected_on_the_cart() {
        Driver.get().get(cartPage.cartUrl);
        Set<String> actualProductTitles = cartPage.productTitlesInCart();
        Assert.assertEquals(expectedProductTitles, actualProductTitles);

    }
}
