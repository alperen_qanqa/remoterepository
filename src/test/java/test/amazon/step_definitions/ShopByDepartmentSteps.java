package test.amazon.step_definitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.openqa.selenium.WebElement;
import test.amazon.pages.Homepage;
import test.amazon.utilities.BrowserUtils;
import test.amazon.utilities.ConfigurationReader;
import test.amazon.utilities.Driver;
import test.amazon.utilities.ExcelUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import static io.restassured.RestAssured.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class ShopByDepartmentSteps {


    //    Main Dept  , List of maps(Subdept name and link)

    @Given("user goes to amazon")
    public void user_goes_to_amazon() {
        Driver.get().get(ConfigurationReader.get("url"));

    }

    @When("user clicks little hamburger menu on the left")
    public void user_clicks_little_hamburger_menu_on_the_left() {

        homepage.AllMenuOntheLeft.click();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    Homepage homepage = new Homepage();

    List<String> mainDepartments = new ArrayList<>();
    List<Map<String, List<Map<String, String>>>> allCategoriesNamesAndLinks = new ArrayList<>();

    @And("clicks on department names one by one")
    public void clicksOnDepartmentNamesOneByOne() throws Exception {
        mainDepartments = homepage.departmentNames();
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data");
        int rowNum = 1;
        //Başlık satırı
        Row header = sheet.createRow(0);
        Cell header1 = header.createCell(0);
        header1.setCellValue("Department");
        Cell header2 = header.createCell(1);
        header2.setCellValue("Subdepartment");
        Cell header3 = header.createCell(2);
        header3.setCellValue("Link");
        Cell header4 = header.createCell(3);
        header4.setCellValue("Status");

        List<String> linksToGo = new ArrayList<>();

        for (String mainDepartment : mainDepartments) {
            homepage.clickOnDepartment(mainDepartment);
            Thread.sleep(100);
            Map<String, List<Map<String, String>>> mainDepartmentData = new LinkedHashMap<>();
            List<Map<String, String>> subDepartmentData = new ArrayList<>();

            for (WebElement subDepartment : homepage.subCategories) {
                Map<String, String> subDepartmentInfo = new LinkedHashMap<>();
                BrowserUtils.scrollToElement(subDepartment);
                if (!subDepartment.getText().trim().equals("")) {
                    String subDeptName = subDepartment.getText().trim();
                    String subDeptLink = subDepartment.getAttribute("href");
                    subDepartmentInfo.put(subDeptName, subDeptLink);
                    subDepartmentData.add(subDepartmentInfo);
                    linksToGo.add(subDeptLink);
                    createExcelFileWith(sheet, rowNum, mainDepartment, subDeptName, subDeptLink);
                    rowNum++;
                }
            }
            mainDepartmentData.put(mainDepartment, subDepartmentData);
            allCategoriesNamesAndLinks.add(mainDepartmentData);
            Thread.sleep(200);
            homepage.clickWithJS(homepage.backToMainMenu);
            Thread.sleep(500);
        }
        for (int i = 0;i<linksToGo.size();i++) {
            Driver.get().get(linksToGo.get(i));
            if (homepage.cart.size()>0) {
                ExcelUtils.setCellData( sheet, "OK", "Status", i + 1);
                System.out.println("OK");
            }
            else {
                ExcelUtils.setCellData( sheet, "Dead link", "Status", i + 1 );
                System.out.println("Dead link");

            }

        }

        FileOutputStream fos = new FileOutputStream(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+"_results.xls");
        workbook.write(fos);
        fos.close();
    }

    public void createExcelFileWith(Sheet sheet, int count, String Department, String SubDepartment, String link) {
        Row row = sheet.createRow(count);
        Cell deptCell = row.createCell(0);
        deptCell.setCellValue(Department);
        Cell subDeptCell = row.createCell(1);
        subDeptCell.setCellValue(SubDepartment);
        Cell linkCell = row.createCell(2);
        linkCell.setCellValue(link);

    }
}
