package test.amazon.api;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;

public class ApiTask {
  /*  @DataProvider
    public Object[][] data() {
        Object[][] data = {
                {5, 10},
                {7, 10},
                {9, 10}
        };
        return data;
    }
    @Test(dataProvider = "data")*/
    public void testPostNumPerUser(int userId, int expectedPostNum) {
        baseURI = "https://jsonplaceholder.typicode.com/posts";
        Response response =
                given().accept(ContentType.JSON).contentType(ContentType.JSON).log().all()
                        .when().get(baseURI);
        Assert.assertEquals(
                Arrays.asList(response.statusCode(), response.contentType()),
                Arrays.asList(200, ContentType.JSON)
        );
        List<Map<String, Object>> actualResponse = response.body().as(List.class);
        int actualPostNum = 0;
        for (Map<String, Object> userData : actualResponse) {
            if (userData.get("userId").equals(userId)) {
                actualPostNum++;
            }
        }
        Assert.assertEquals(actualPostNum, expectedPostNum);
    }

    public void arePostIdsUnique() {
        baseURI = "https://jsonplaceholder.typicode.com/posts";
        Response response =
                given().accept(ContentType.JSON).contentType(ContentType.JSON).log().all()
                        .when().get(baseURI);
        Assert.assertEquals(
                Arrays.asList(response.statusCode()),
                Arrays.asList(200)
        );
        List<Integer> postIds = response.body().jsonPath().getList("id", Integer.class);
        System.out.println(postIds);
        for (int i = 0; i < postIds.size(); i++) {
            if (postIds.indexOf(postIds.get(i)) != postIds.lastIndexOf(postIds.get(i))) {
                Assert.fail("There are duplicate post ids");
            }
        }
    }



}
