package test.amazon.utilities;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Excel {
    public static void main(String[] args) throws Exception {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data");
        FileOutputStream fos = new FileOutputStream("file.xls");
        //Başlık Satır
        Row row = sheet.createRow(0);

        Cell header1 = row.createCell(0);
        header1.setCellValue("Department");
        Cell header2 = row.createCell(1);
        header2.setCellValue("Subdepartment");
        Cell header3 = row.createCell(2);
        header3.setCellValue("Link");
        Cell header4 = row.createCell(3);
        header4.setCellValue("Status");

        Cell cell;
        Row row1;

        try {
            row1 = sheet.getRow(1);
            cell = row.getCell(4);

            if (cell == null) {
                cell = row.createCell(4);
                cell.setCellValue("OK");
            } else {
                cell.setCellValue("OK");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        workbook.write(fos);
        fos.close();

    }
//    public static void setCellData(Workbook workBook, Sheet workSheet, String value, int rowNum, int colNum,FileOutputStream fos) {
//        Cell cell;
//        Row row;
//
//        try {
//            row = workSheet.getRow(rowNum);
//            cell = row.getCell(colNum);
//
//            if (cell == null) {
//                cell = row.createCell(colNum);
//                cell.setCellValue(value);
//            } else {
//                cell.setCellValue(value);
//            }
//
//            //workBook.write(fos);
//            //fos.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void setCellData(Workbook workBook, Sheet workSheet,String value, String columnName, int row,FileOutputStream fos) {
//        int column = getColumnsNames(workSheet).indexOf(columnName);
//        setCellData(workBook,workSheet,value, row, column,fos);
//    }
//
//    public static List<String> getColumnsNames(Sheet workSheet) {
//        List<String> columns = new ArrayList<>();
//
//        for (Cell cell : workSheet.getRow(0)) {
//            columns.add(cell.toString());
//        }
//        return columns;
//    }
}
