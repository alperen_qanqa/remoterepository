@ui
Feature: Amazon Search Functionality

  Scenario: Search "Laptop" in Amazon website and add laptops to the cart
    Given user searches laptop in amazon.com
    When user adds all non-discounted ones on the first page of results to the cart
    Then user should see what is exactly selected on the cart

