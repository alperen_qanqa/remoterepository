$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/Api.feature");
formatter.feature({
  "name": "Api Task",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@api"
    }
  ]
});
formatter.scenarioOutline({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Http Get request is sent to \"https://jsonplaceholder.typicode.com/posts\"",
  "keyword": "Given "
});
formatter.step({
  "name": "Status Code is 200",
  "keyword": "When "
});
formatter.step({
  "name": "According to response, user \u003cuserId\u003e should have \u003cnumposts\u003e posts",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "userId",
        "numposts"
      ]
    },
    {
      "cells": [
        "5",
        "10"
      ]
    },
    {
      "cells": [
        "7",
        "10"
      ]
    },
    {
      "cells": [
        "9",
        "10"
      ]
    }
  ]
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@api"
    }
  ]
});
formatter.step({
  "name": "Http Get request is sent to \"https://jsonplaceholder.typicode.com/posts\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ApiStepDefs.http_Get_request_is_sent_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Status Code is 200",
  "keyword": "When "
});
formatter.match({
  "location": "ApiStepDefs.status_Code_is(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "According to response, user 5 should have 10 posts",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiStepDefs.according_to_response_user_should_have_posts(int,int)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@api"
    }
  ]
});
formatter.step({
  "name": "Http Get request is sent to \"https://jsonplaceholder.typicode.com/posts\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ApiStepDefs.http_Get_request_is_sent_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Status Code is 200",
  "keyword": "When "
});
formatter.match({
  "location": "ApiStepDefs.status_Code_is(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "According to response, user 7 should have 10 posts",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiStepDefs.according_to_response_user_should_have_posts(int,int)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@api"
    }
  ]
});
formatter.step({
  "name": "Http Get request is sent to \"https://jsonplaceholder.typicode.com/posts\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ApiStepDefs.http_Get_request_is_sent_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Status Code is 200",
  "keyword": "When "
});
formatter.match({
  "location": "ApiStepDefs.status_Code_is(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "According to response, user 9 should have 10 posts",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiStepDefs.according_to_response_user_should_have_posts(int,int)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Uniqueness of post Ids",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@api"
    }
  ]
});
formatter.step({
  "name": "Http Get request is sent to \"https://jsonplaceholder.typicode.com/posts\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ApiStepDefs.http_Get_request_is_sent_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Status Code is 200",
  "keyword": "When "
});
formatter.match({
  "location": "ApiStepDefs.status_Code_is(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Each post id should be unique",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiStepDefs.eachPostIdShouldBeUnique()"
});
formatter.result({
  "status": "passed"
});
});